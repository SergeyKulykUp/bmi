package com.sergeykulyk.bmi.ui.personal_details

import com.sergeykulyk.bmi.data.models.ResultModel

class PersonalDetailsPresenter(private val personalDetailsView: PersonalDetailsView) {

    fun calculateIndex(name: String, weight: Int, height: Int, gender: Int) {
        if (!isNameValid(name)) return

        val resultModel = ResultBuilder()
                .setName(name)
                .setWeight(weight)
                .setHeight(height)
                .setGender(gender)
                .build()

        personalDetailsView.openResultScreen(resultModel)
    }

    private fun isNameValid(name: String): Boolean {
        val isNameEmpty = name.isEmpty()
        if (isNameEmpty) {
            personalDetailsView.seNameError()
        }
        return !isNameEmpty
    }

    interface PersonalDetailsView {
        fun openResultScreen(resultModel: ResultModel)

        fun seNameError()
    }
}