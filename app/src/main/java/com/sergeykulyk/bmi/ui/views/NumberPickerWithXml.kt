package com.sergeykulyk.bmi.ui.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.NumberPicker
import androidx.core.content.res.ResourcesCompat
import com.sergeykulyk.bmi.R

class NumberPickerWithXml : NumberPicker {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        processXmlAttributes(attrs, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        processXmlAttributes(attrs, defStyleAttr, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        processXmlAttributes(attrs, defStyleAttr, defStyleRes)
    }

    private fun processXmlAttributes(attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) {
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.NumberPickerWithXml, defStyleAttr, defStyleRes)

        try {
            this.minValue = attributes.getInt(R.styleable.NumberPickerWithXml_minValue, 0)
            this.maxValue = attributes.getInt(R.styleable.NumberPickerWithXml_maxValue, 0)
            this.value = attributes.getInt(com.sergeykulyk.bmi.R.styleable.NumberPickerWithXml_defaultValue, 0)
        } finally {
            attributes.recycle()
        }

    }

    private lateinit var type: Typeface

    override fun addView(child: View) {
        super.addView(child)
        updateView(child)
    }

    override fun addView(child: View, index: Int,
                         params: android.view.ViewGroup.LayoutParams
    ) {
        super.addView(child, index, params)

        updateView(child)
    }

    override fun addView(child: View, params: android.view.ViewGroup.LayoutParams) {
        super.addView(child, params)

        updateView(child)
    }

    private fun updateView(view: View) {

        if (view is EditText) {
            view.textSize = 14f

            view.setTextColor(resources.getColor(R.color.colorBlue))
        }
    }
}