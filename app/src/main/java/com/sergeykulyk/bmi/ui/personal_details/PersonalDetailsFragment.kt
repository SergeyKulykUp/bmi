package com.sergeykulyk.bmi.ui.personal_details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.sergeykulyk.bmi.R
import com.sergeykulyk.bmi.data.models.ResultModel
import com.sergeykulyk.bmi.ui.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_personal_details.*

class PersonalDetailsFragment : Fragment(), PersonalDetailsPresenter.PersonalDetailsView {

    private lateinit var presenter: PersonalDetailsPresenter
    private var mInterstitialAd: InterstitialAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = PersonalDetailsPresenter(this)

        initInterstitialAd()
    }

    private fun initInterstitialAd() {
        mInterstitialAd = InterstitialAd(context)
        mInterstitialAd?.adUnitId = getString(R.string.interstitial_ad_id)
        mInterstitialAd?.adListener = object : AdListener() {
            override fun onAdFailedToLoad(p0: Int) {
                super.onAdFailedToLoad(p0)
                mInterstitialAd?.loadAd(AdRequest.Builder().build())
                navigateToReusult()
            }

            override fun onAdClosed() {
                super.onAdClosed()
                mInterstitialAd?.loadAd(AdRequest.Builder().build())
                navigateToReusult()
            }
        }
        mInterstitialAd?.loadAd(AdRequest.Builder().build())
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_personal_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()

        calculate.setOnClickListener {
            if (mInterstitialAd?.isLoaded!!) {
                mInterstitialAd?.show()
            } else {
                Log.d("TAG", "The interstitial wasn't loaded yet.")
            }
        }

        initGenderPickerValues()
    }

    private fun initToolbar() {
        (activity as MainActivity).back?.visibility = View.GONE
        (activity as MainActivity).toolbarTitle.text = getString(R.string.add_bmi_details)
    }

    private fun navigateToReusult() {
        presenter.calculateIndex(
                name.text.toString(),
                weightPicker.value,
                heightPicker.value,
                genderPicker.value
        )
    }

    override fun seNameError() {
        name.error = context?.resources?.getString(R.string.error_empty_name)
    }

    override fun openResultScreen(resultModel: ResultModel) {
        findNavController().navigate(R.id.action_personalDetailsFragment_to_resultFragment, Bundle().apply {
            putSerializable(ResultModel.TAG, resultModel)
        })
    }

    private fun initGenderPickerValues() {
        genderPicker.minValue = 0
        genderPicker.maxValue = 1
        genderPicker.displayedValues = arrayOf(
                getString(R.string.male),
                getString(R.string.female)
        )
    }
}
