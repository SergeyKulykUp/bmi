package com.sergeykulyk.bmi.ui.result

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sergeykulyk.bmi.APP_LINK
import com.sergeykulyk.bmi.PLAY_STORE_RATE_US
import com.sergeykulyk.bmi.R
import com.sergeykulyk.bmi.data.models.ResultModel
import com.sergeykulyk.bmi.ui.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_result.*

class ResultFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()

        shareLayout.setOnClickListener { share() }
        rateLayout.setOnClickListener { rateUs() }

        setResult()
    }

    private fun initToolbar() {
        (activity as MainActivity).back?.visibility = View.VISIBLE
        (activity as MainActivity).toolbarTitle.text = getString(R.string.bmi_details)
    }

    private fun share() {
        val intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(
                    Intent.EXTRA_TEXT,
                    String.format(resources.getString(R.string.share_friend), APP_LINK)
            )
            type = "text/plain"
        }
        startActivity(Intent.createChooser(intent, getString(R.string.share_with)))
    }

    private fun rateUs() {
        val uri = Uri.parse(PLAY_STORE_RATE_US)
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse(APP_LINK)))
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setResult() {
        val resultModel = arguments?.getSerializable(ResultModel.TAG) as ResultModel?

        resultModel?.let {
            indexInt.text = it.index.toString()
            indexFloat.text = ".${it.floatIndex}"
            resultText.text = it.resultText
            rangeResult.text = it.rangeText
            ponderalIndex.text = it.ponderalText
        }
    }
}
