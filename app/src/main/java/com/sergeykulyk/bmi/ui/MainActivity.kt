package com.sergeykulyk.bmi.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.sergeykulyk.bmi.R.layout.activity_main)

        back.setOnClickListener { onBackPressed() }
        adView.loadAd(AdRequest.Builder().build())
    }
}
