package com.sergeykulyk.bmi.ui.personal_details

import com.sergeykulyk.bmi.R
import com.sergeykulyk.bmi.data.models.ResultModel
import com.sergeykulyk.bmi.getString
import java.lang.IndexOutOfBoundsException

class ResultBuilder {

    private var name: String = ""
    private var weight: Int = 1
    private var height: Int = 1
    private var gender: Int = 1

    private var bmi: Float = 1f

    fun setName(name: String): ResultBuilder {
        this.name = name
        return this
    }

    fun setWeight(weight: Int): ResultBuilder {
        this.weight = weight
        return this
    }

    fun setHeight(height: Int): ResultBuilder {
        this.height = height
        return this
    }

    fun setGender(gender: Int): ResultBuilder {
        this.gender = gender
        return this
    }

    private fun calculateBMI(): Float {
        val heightValue = height / 100f
        return weight / (heightValue * heightValue)
    }

    fun build(): ResultModel {
        bmi = calculateBMI()

        val resultModel = ResultModel()

        resultModel.index = buildIndex()
        resultModel.floatIndex = buildFloatIndex()
        resultModel.resultText = buildResultText()
        resultModel.rangeText = buildRangeText()
        resultModel.ponderalText = buildPonderalText()

        return resultModel
    }

    private fun buildIndex(): Int {
        return bmi.toInt()
    }

    private fun buildFloatIndex(): Int {
        val floatIndex = (bmi - bmi.toInt()).toString()
        return try {
            if (floatIndex.length > 3) {
                floatIndex.substring(2, 4).toInt()
            } else {
                floatIndex[2].toInt()
            }
        } catch (ignore: IndexOutOfBoundsException) {
            0
        }
    }

    private fun buildResultText(): String {
        val result = getBMIText()
        return String.format(getString(R.string.result_text), name, result)
    }


    private fun buildRangeText(): String {
        val bmiText = getBMIText()
        val rangeValues = getRangeValues()

        return String.format(
                getString(R.string.result_range_text),
                bmiText,
                rangeValues[0],
                rangeValues[1]
        )
    }

    private fun getRangeValues(): MutableList<String> {
        val startRange: Float
        val endRange: Float

        val pair = if (gender == 0) {
            getMaleRangeValues()
        } else {
            getFemaleRangeValues()
        }

        startRange = pair.first
        endRange = pair.second

        val startValue = startRange.toString()

        var endValue = endRange.toString()
        if (endRange == 0f) {
            endValue = "n"
        }

        val rangeValues = mutableListOf<String>()

        rangeValues.add(startValue)
        rangeValues.add(endValue)

        return rangeValues
    }

    private fun getMaleRangeValues(): Pair<Float, Float> {
        var startRange = 0f
        var endRange = 0f

        if (bmi.compareTo(MALE_UNDERWEIGHT) <= 0) {
            endRange = MALE_UNDERWEIGHT
        } else if (bmi.compareTo(MALE_UNDERWEIGHT) > 0 && bmi.compareTo(MALE_NORMAL) <= 0) {
            startRange = MALE_UNDERWEIGHT
            endRange = MALE_NORMAL
        } else if (bmi.compareTo(MALE_NORMAL) > 0 && bmi.compareTo(MALE_OVERWEIGHT) <= 0) {
            startRange = MALE_NORMAL
            endRange = MALE_OVERWEIGHT
        } else {
            startRange = MALE_OVERWEIGHT
        }
        return Pair(startRange, endRange)
    }

    private fun getFemaleRangeValues(): Pair<Float, Float> {
        var startRange = 0f
        var endRange = 0f

        if (bmi.compareTo(FEMALE_UNDERWEIGHT) <= 0) {
            endRange = FEMALE_UNDERWEIGHT
        } else if (bmi.compareTo(FEMALE_UNDERWEIGHT) > 0 && bmi.compareTo(FEMALE_NORMAL) <= 0) {
            startRange = FEMALE_UNDERWEIGHT
            endRange = FEMALE_NORMAL
        } else if (bmi.compareTo(FEMALE_NORMAL) > 0 && bmi.compareTo(FEMALE_OVERWEIGHT) <= 0) {
            startRange = FEMALE_NORMAL
            endRange = FEMALE_OVERWEIGHT
        } else {
            startRange = FEMALE_OVERWEIGHT
        }
        return Pair(startRange, endRange)
    }

    // Use https://www.google.com/search?q=ponderal+index&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiPxbyZwfngAhWq1aYKHd24Ca0Q_AUIDigB&biw=1853&bih=949#imgrc=VgzZUEYNwWEJMM:
    private fun buildPonderalText(): String {
        val ponderalIndex = 100 * 1000 * weight / Math.pow(height.toDouble(), 3.0)
        return String.format(getString(R.string.result_ponderal_index_text), ponderalIndex)
    }

    private fun getBMIText(): String {
        return if (gender == 0) {
            getMaleBmiText()
        } else {
            getFemaleBmiText()
        }
    }

    private fun getMaleBmiText(): String {
        return if (bmi.compareTo(MALE_UNDERWEIGHT) <= 0) {
            getString(R.string.underweight)
        } else if (bmi.compareTo(MALE_UNDERWEIGHT) > 0 && bmi.compareTo(MALE_NORMAL) <= 0) {
            getString(R.string.normal)
        } else if (bmi.compareTo(MALE_NORMAL) > 0 && bmi.compareTo(MALE_OVERWEIGHT) <= 0) {
            getString(R.string.overweight)
        } else {
            getString(R.string.obese_class)
        }
    }

    private fun getFemaleBmiText(): String {
        return if (bmi.compareTo(FEMALE_UNDERWEIGHT) <= 0) {
            getString(R.string.underweight)
        } else if (bmi.compareTo(FEMALE_UNDERWEIGHT) > 0 && bmi.compareTo(FEMALE_NORMAL) <= 0) {
            getString(R.string.normal)
        } else if (bmi.compareTo(FEMALE_NORMAL) > 0 && bmi.compareTo(FEMALE_OVERWEIGHT) <= 0) {
            getString(R.string.overweight)
        } else {
            getString(R.string.obese_class)
        }
    }

    companion object {
        const val MALE_UNDERWEIGHT = 18.2f
        const val MALE_NORMAL = 25.6f
        const val MALE_OVERWEIGHT = 28.9f

        const val FEMALE_UNDERWEIGHT = 17.5f
        const val FEMALE_NORMAL = 25.7f
        const val FEMALE_OVERWEIGHT = 30.3f
    }
}