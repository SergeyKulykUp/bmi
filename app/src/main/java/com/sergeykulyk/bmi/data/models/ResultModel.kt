package com.sergeykulyk.bmi.data.models

import java.io.Serializable

class ResultModel : Serializable {
    var index: Int = 0
    var floatIndex: Int = 0
    var resultText: String = ""
    var rangeText: String = ""
    var ponderalText: String = ""

    companion object {
        const val TAG = "result_model"
    }
}
