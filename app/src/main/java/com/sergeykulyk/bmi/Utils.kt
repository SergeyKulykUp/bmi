package com.sergeykulyk.bmi

fun getString(stringId: Int): String = getAppContext().resources.getString(stringId)