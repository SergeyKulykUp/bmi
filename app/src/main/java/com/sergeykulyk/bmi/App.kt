package com.sergeykulyk.bmi

import android.app.Application

class App : Application() {
    companion object {
        internal lateinit var app: App
    }

    init {
        app = this
    }
}

fun getAppContext(): App = App.app